var mongoose = require('mongoose');



var TradeSchema = new mongoose.Schema({
	tradedate: {
        type: Date,
		default: Date.now
    },
    tradeprice: {
		type: Number,
		default: 0,
    },
    tradevolume: {
		type: Number,
		default: 0,
    },
    tradetype: {
		type: String,
		uppercase: true,
		default: ""
    },
	stockname:{
		type: String,
		uppercase: true,
		default: ""
	}

});

module.exports = mongoose.model('Trade', TradeSchema);
