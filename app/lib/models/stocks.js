var mongoose = require('mongoose');



var StockSchema = new mongoose.Schema({
    stockname: {
        type: String,
		uppercase: true,
		default: ''
    }

});

module.exports = mongoose.model('Stocks', StockSchema);
