//the models
var Stocks = require('./models/stocks');
var Trade = require('./models/trade');

//=========================== utility functions begin ============================//

//takes in a list of trade and computes how many stocks were bought, remaining, etc
//for each type of stock traded
function tradeSummary(trades){
	var accounting = {};
	//get a list of stocks traded 
	for(var i = 0; i < trades.length; i++) {
		//the format for storage is (stocks remaining, bought, total price payed,total earned
		accounting[trades[i].stockname] = [0,0,0,0];
	}
	
	//now that we now what all stocks were bought/sold
	//we shall do some accouting
	for(var i = 0; i < trades.length; i++) {
		holding = parseInt(accounting[trades[i].stockname][0]);
		bought = parseInt(accounting[trades[i].stockname][1]);
		pricepayed = parseInt(accounting[trades[i].stockname][2]);
		moneyearned = parseInt(accounting[trades[i].stockname][3]);
		var isSell = ( trades[i].tradetype.toString() === 'SELL');
		if(isSell){//sell
			holding -= trades[i].tradevolume;
			moneyearned += (trades[i].tradevolume  * trades[i].tradeprice);
		}else{//buy
			holding += trades[i].tradevolume;
			bought += trades[i].tradevolume;
			pricepayed += (trades[i].tradevolume * trades[i].tradeprice);
		}	
		accounting[trades[i].stockname] = [holding,bought,pricepayed,moneyearned];
	}
	return accounting;
};

//takes in a list of trades and computes the holdings for each stock
function computeHoldings(trades){

	var holdings = {};
	var accounting = tradeSummary(trades);
	for(var a in accounting){
		//the format is remaining stocks and price
		holdings[a] = [0,0];
		holdings[a][0] = parseInt(accounting[a][0]);
		//while computing price, we do not consider sells, only buys
		holdings[a][1] = parseInt(accounting[a][2])/parseInt(accounting[a][1]);
	}
	return holdings;
}

//vaidates the user proivded data for empty, improper fields 
function validateTradeData(req){
	var message = "";
	if(req.query.tradeprice == undefined){
		message += "tradeprice cannot be empty.";
	}else{
		if(isNaN(req.query.tradeprice)){
			message += "tradeprice must be a number.";
		}
	}
	if(req.query.tradevolume == undefined){
		message += "tradevolume cannot be empty.";
	}else{
		if(isNaN(req.query.tradevolume)){
			message += "tradevolume must be a number.";
		}
	}
	if(req.query.tradetype == undefined){
		message += "tradetype cannot be empty.";
	}else{
		if(req.query.tradetype !== "BUY" && req.query.tradetype !== "SELL"){
			message += "tradetype can have only two value; BUY or SELL.";
		}
	}
	return message;

}

//vaidates the user proivded tradeid 
function validateTradeId(tradeid,trades){
		var allTradeIds_str = "";
		var allTradeIds = new Set();
		var message = "";
		for(let i=0; i<trades.length; i++){
			allTradeIds.add(trades[i]._id.toString());
			allTradeIds_str += trades[i]._id + ", ";
		}

		if(tradeid == undefined){
			message = "Trade id must be provided in order to update/remove a trade. ";
			message += "Ids of existing trades are "+allTradeIds_str;
			return message;
		}
		if(!allTradeIds.has(tradeid)){
			message = "Trade id provided does not match an existing trade. ";
			message += "Ids of existing trades are "+allTradeIds_str;
			return message;
		}
	return message;
}

//=========================== utility functions end ============================//


//================== publicly accessible functionality begin ====================//

exports.getHoldings = function(res){

	var json = {};
	json.success = false;
	json.data = {};
	Trade.find(function (err,trades){
        if (err) {
			json.data.holdings = null;
        }else{
			json.success = true;
			json.data.holdings = computeHoldings(trades);
		};
		res.json(json);
	});
}


exports.getPortfolio = function(res){

	var json = {};
	json.success = false;
	json.data = {};
	Trade.find(function (err,trades){
        if (err) {
			json.success = false;
			json.data.holdings = null;
        }else{
			//compile a list of trades
			json.success = true;	
			for(var i = 0; i < trades.length; i++) {
				if(!(trades[i].stockname in json)){
					json.data[trades[i].stockname] = [];
				}	
				json.data[trades[i].stockname].push(trades[i]);
			}
			json.data.holdings = computeHoldings(trades);
		};
		res.json(json);
	});
}

//the formular used for returns calculation is as follows:
//returns = money_earned - money_payed
exports.getReturns = function(res){
	var finalPrice = 100;//the final price is fixed in our case
	var json = {};
	json.success = false;
	json.data = {};
	Trade.find(function (err,trades){
        if (err) {
			json.success = false;
			json.data.holdings = null;
        }else{
			json.success = true;
			accounting = tradeSummary(trades);
			var cmReturns = 0;
			for(var a in accounting){
				//returns = money earned by selling + value of stock - price payed
				var returns = parseInt(accounting[a][3]) + (parseInt(accounting[a][0]) * finalPrice) - parseInt(accounting[a][2]); 
				cmReturns += returns;
			}
			json.data.cummulativereturns = cmReturns;
		};
		res.json(json);
	});
}


exports.addTrade = function(req,res){
	var json = {};
	json.success = false;
	json.data = {};
	var message = "";

	//validate trade data before adding entry
	message += validateTradeData(req);
	
	if(req.query.stockname == undefined || message.length >0){
		message += "stockname cannot be empty.";
		json.data.message = message;
		res.json(json);
		return res;
	}else{
		//check if new trade is for a valid stock
		Stocks.find(function(err, stocks){
			if(err){
				message += "cannot fetch stock details for trading now. Try again later.";
				json.data.message = message;
				res.json(json);
				return res;
			}else{
				if(stocks.length == 0){
					message += "No stocks currently available for trading. Try again later.";
					json.data.message = message;
					res.json(json);
					return res;
				}else{
					var allStocks = new Set();
					var stocknames = ""; 
					for(let i=0; i<stocks.length; i++){
						allStocks.add(stocks[i].stockname);
						stocknames += stocks[i].stockname +", ";
					}
					if(!allStocks.has(req.query.stockname)){
						message += "Sorry "+req.query.stockname+" is currently not available for trading. ";
						message += "Only stocks available for trading are "+ stocknames;
						json.data.message = message;
						res.json(json);
						return res;
					}else{
    					Trade.create({
							stockname: req.query.stockname,
							tradeprice: parseInt(req.query.tradeprice),
							tradevolume: parseInt(req.query.tradevolume),
							tradetype: req.query.tradetype
							//done: false
						}, function (err, portfolio) {
       						if (err){
								message = "error adding trade, please try again";
							}else{
								message = "trade added successfully";
							}
							json.success = true;
							json.data.message = message;
							res.json(json);
							return res;
        				});
					}
				}
			}
		});
	}
};

exports.updateTrade = function(req,res){
	var json = {};
	json.success = false;
	json.data = {};
	var message = "";
	
	//get the list available trade ids in case we have to tell the user
	Trade.find(function (err,trades){
        if (err) {
			json.data.message = "Sorry cannot process update requests at the moment";
			res.json(json);
			return res;
        }else{
			//validate the trade id
			var idMsg = validateTradeId(req.query.tradeid,trades);	
			if(idMsg.length>0)	{
				json.data.messge = idMsg;
				res.json(json);
				return res;
			}		
			//validate remaining trade data 
			message += validateTradeData(req);
			
			if(req.query.stockname != undefined){
				message += "stockname cannot be changed for trade updates.";
			}
			if(message.length >0){
				json.data.message = message;
				res.json(json);
				return res;
			}
				//data validated go ahead with update
				Trade.update( 
					{_id: req.query.tradeid},//update crieteria
					{
						tradeprice: req.query.tradeprice,
						tradevolume: req.query.tradevolume,
						tradetype: req.query.tradetype
					}, function (err, todo) {
						if (err){
							json.data.message = "Error updating trade with id :"+req.query.tradeid;
						}else{
							json.success = true;
							json.data.message = "successfully updating trade with id : "+req.query.tradeid;
						}
					res.json(json);
				});
		}
	});	
};


exports.removeTrade = function(req,res){
	var json = {};
	json.success = false;
	json.data = {};
	var message = "";
	Trade.find(function (err,trades){
        if (err) {
			json.data.message = "Sorry cannot process delete requests at the moment";
			res.json(json);
			return res;
        }else{
			//validate the trade id
			var idMsg = validateTradeId(req.query.tradeid,trades);	
			if(idMsg.length>0)	{
				json.data.messge = idMsg;
				res.json(json);
				return res;
			}		

				//id validated go ahead with removal
				Trade.remove({
					_id: req.query.tradeid
				}, function (err, todo) {
					if (err){
						json.data.message = "Error deleting trade with id :"+req.query.tradeid;
					}else{
						json.success = true;
						json.data.message = "successfully deleted trade with id : "+req.query.tradeid;
					}
					res.json(json);
				});
		}
	});	
};
