//the functionality
var SmCApp = require('./lib/func.js');

//accessible paths
module.exports = function (app) {

    app.get('/portfolio', function (req, res) {
        SmCApp.getPortfolio(res);
    });
    app.get('/holdings', function (req, res) {
        SmCApp.getHoldings(res);
    });
    app.get('/returns', function (req, res) {
        SmCApp.getReturns(res);
    });
    app.post('/addTrade', function (req, res) {
		SmCApp.addTrade(req,res);
    });
    app.post('/updateTrade', function (req, res) {
		SmCApp.updateTrade(req,res);
    });
    app.post('/removeTrade', function (req, res) {
		SmCApp.removeTrade(req,res);
    });

    // application -------------------------------------------------------------
    app.get('*', function (req, res) {
        SmCApp.getPortfolio(res);
    });
};
